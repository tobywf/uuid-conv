def uuid(value: str) -> bytes:
    """Serializes a string UUID into bytes with a Microsoft COM-compatible
    mixed-endian format.

    Call :meth:`bytes.hex` on the return value to convert into a string of
    hexadecimal digits.

    See also Visual Studio's `uuid attribute <https://docs.microsoft.com/en-us/cpp/cpp/uuid-cpp>`_.

    >>> guid = "B3A6F3E0-2B43-11CF-A2DE-00AA00B93356"
    >>> uuid(guid).hex()
    'e0f3a6b3432bcf11a2de00aa00b93356'
    """

    def rev(v: bytes) -> bytes:
        return bytes(reversed(v))

    time_low, time_mid, time_hi, clock_seq, node = [
        bytes.fromhex(v) for v in value.split("-")
    ]
    return b"".join([rev(time_low), rev(time_mid), rev(time_hi), clock_seq, node])


if __name__ == "__main__":
    import sys

    print(*[f"{b:02x}" for b in uuid(sys.argv[1])])
