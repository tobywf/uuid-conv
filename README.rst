uuid_conv
=========

Serializes a string UUID into bytes with a Microsoft COM-compatible mixed-endian format. Useful for reverse-engineering COM code.

No dependencies (other than Python 3). Also provides an entrypoint for CLI usage:

.. code-block:: console

    $ python3 -m uuid_conv "B3A6F3E0-2B43-11CF-A2DE-00AA00B93356"
    3356"
    e0 f3 a6 b3 43 2b cf 11 a2 de 00 aa 00 b9 33 56

Development
-----------

.. code-block:: console

    $ pip install -r requirements-dev.txt
    $ black uuid_conv.py
    $ mypy --strict uuid_conv.py
    $ pytest --doctest-modules --cov uuid_conv uuid_conv.py
    $ sphinx-build -b singlehtml . _build
